<?php include('controllers/empleados.php'); ?>
<?php include('../template/header.php'); ?>
<div class="container">
  <h1>Crear empleado</h1>
  <div class="alert alert-info" role="alert">Los campos con (*) son obligatorios</div>
  <form class="form-horizontal" id="frm">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Nombre completo *</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre completo del empleado" required>
      </div>
    </div>

    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Correo electrónico *</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" name="email" placeholder="Correo electrónico" required>
      </div>
    </div>

    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Sexo *</label>
      <div class="col-sm-10">
        <div class="radio">
          <label>
            <input type="radio" id="sexom" name="sexo" value="M" required> Masculino
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" id="sexof" name="sexo" value="F" required> Femenino
          </label>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Area *</label>
      <div class="col-sm-10">
        <select class="form-control" name="area_id" id="area_id" required style="width: 100%;" required>
          <option value="">Seleccionar</option>
          <?php foreach ($areas as $key => $value) { ?>
            <option value="<?php echo $value['id'] ?>"><?php echo $value['nombre'] ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Descripción *</label>
      <div class="col-sm-10">
        <textarea id="descripcion" name="descripcion" class="form-control" rows="3" placeholder="Descripción de la experiencia del empleado" required></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label"></label>
      <div class="col-sm-10">
        <div class="checkbox">
          <label>
            <input type="checkbox" id="boletin" name="boletin"> Deseo recibir boletín informativo
          </label>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Roles *</label>
      <div class="col-sm-10">
        <?php foreach ($roles as $key => $value) { ?>
          <div class="checkbox">
            <label>
              <input class="roles" type="checkbox" id="<?php echo 'rol' . $value['id'] ?>" name="<?php echo 'rol' . $value['id'] ?>"> <?php echo $value['nombre'] ?>
            </label>
          </div>
        <?php } ?>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </form>
  <!-- Fin formulario crear empleado -->
    <hr>
  <!-- Inicio listar empleados -->
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th> <i class="fa fa-user fa-fw"></i> Nombre</th>
        <th> <i class="fa fa-at fa-fw"></i> Email</th>
        <th> <i class="fa fa-venus-mars fa-fw"></i> Sexo</th>
        <th> <i class="fa fa fa-briefcase fa-fw"></i> Área</th>
        <th> <i class="fa fa fa-envelope fa-fw"></i> Boletín</th>
        <th>Modificar</th>
        <th>Eliminar</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($empleados as $key => $value) {?>
      <tr>
        <td><?php echo $value['nombre'] ?></td>
        <td><?php echo $value['email'] ?></td>
        <td><?php echo $value['sexo'] ?></td>
        <td><?php echo $value['area'] ?></td>
        <td><?php echo $value['boletin'] ?></td>
        <td> <a style="color: black;" href="modificar.php?opcn=editar&id=<?php echo $value['id'] ?>"><i class="fa fa-pencil-square-o fa-fw"></i> </a>  </td>
        <td> <a style="color: black;" href="" class="eliminar" data-id="<?php echo $value['id'] ?>"><i class="fa fa-trash fa-fw"></i> </a> </td>
      </tr>
      <?php } ?>
    </tbody>
  </table>
</div>
<?php include('../template/footer.php'); ?>
<script>
let opcn = "crear"
let id = '';
</script>
<script src="js/empleados.js?sin_cache=<?php echo md5(time()); ?>"></script>