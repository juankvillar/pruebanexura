<?php

Class empleados{

	public function getAreas(){
		include_once('../config/init_db.php');
		$query = "SELECT * FROM areas;";
		$user = DB::query($query);
		return $user;
	}

	public function getRoles(){
		include_once('../config/init_db.php');
		$query = "SELECT * FROM roles;";
		$user = DB::query($query);
		return $user;
	}

	public  function getEmpleados(){
		include_once('../config/init_db.php');
		$query = "SELECT e.*, a.nombre area FROM empleados e inner join areas a on e.area_id = a.id;";
		$user = DB::query($query);
		return $user;
	}

	public  function getEmpleado( $id ){
		include_once('../config/init_db.php');
		$query = "SELECT * FROM empleados where id = $id;";
		$user = DB::query($query);		
		return $user[0];
	}

	public function getRolesEmpleado( $id ){
		include_once('../config/init_db.php');
		$query = "SELECT r.*, er.empleado_id from roles r
					LEFT JOIN empleado_rol er
						on r.id = er.rol_id
							and er.empleado_id = $id";
		$user = DB::query($query);
		return $user;
	}

	public  function insertEmpleaado( $p ){
		$datos = array();
		foreach ($p['datos'] as $key => $value) {
			$datos[$value['name']] = $value['value'];
		}
		extract( $datos );
		include_once('../../config/init_db.php');

		$boletin = $boletin == 'on' ? 1 : 0 ; 
		$query = "INSERT INTO empleados
						(
						nombre,
						email,
						sexo,
						area_id,
						boletin,
						descripcion
						)
						VALUES
						(
						'$nombre',
						'$email',
						'$sexo',
						'$area_id',
						'$boletin',
						'$descripcion'
						);";
		$res_query = DB::query($query);
		$empleado_id = DB::insertId();
				
		$data = array();
		if( $res_query ){
			foreach ($p['roles'] as $key => $value) {
				if( $value['value'] == 'false' ){continue;}
				$rol_id = str_replace("rol", "", $value['name']);
				$query2 = "INSERT INTO empleado_rol
								(
								empleado_id,
								rol_id
								)
								VALUES
								(
								$empleado_id,
								$rol_id
								);";
				$res_query = DB::query($query2);
			}
			$data['error'] 	= false;
			$data['msj'] 	= "Empleado creado correctamente" ;
			$data['type'] 	= "success" ;
		}else{
			$data['error'] 	= true;
			$data['msj']	= "No se pudo crear el empleado";
			$data['type'] 	= "error" ;
		}
		return $data;
	}

	public  function updateEmpleado( $p ){
		$datos = array();
		foreach ($p['datos'] as $key => $value) {
			$datos[$value['name']] = $value['value'];
		}
		extract( $datos );
		include_once('../../config/init_db.php');

		$boletin = $boletin == 'on' ? 1 : 0 ; 
		$query = "UPDATE empleados
							SET
							nombre 	= '$nombre',
							email 	= '$email',
							sexo 	= '$sexo',
							area_id = '$area_id',
							boletin = '$boletin',
							descripcion = '$descripcion'
							WHERE id = $id;";
		$res_query = DB::query($query);

		$data = array();
		if( $res_query ){
			$query_del = "delete from empleado_rol where empleado_id = $id;";
			DB::query($query_del);

			foreach ($p['roles'] as $key => $value) {
				if( $value['value'] == 'false' ){continue;}
				$rol_id = str_replace("rol", "", $value['name']);
				$query2 = "INSERT INTO empleado_rol
								(
								empleado_id,
								rol_id
								)
								VALUES
								(
								$id,
								$rol_id
								);";
				$res_query = DB::query($query2);
			}
			$data['error'] 	= false;
			$data['msj'] 	= "Empleado actualizado correctamente" ;
			$data['type'] 	= "success" ;
		}else{
			$data['error'] 	= true;
			$data['msj']	= "No se pudo actualizar el empleado";
			$data['type'] 	= "error" ;
		}
		return $data;
	}

	public  function deletedEmpleado( $id ){
		include_once('../../config/init_db.php');
		$query = "DELETE FROM empleados where id = $id;";
		$res_query = DB::query($query);

		$query = "DELETE FROM empleado_rol where empleado_id = $id;";
		$res_query = DB::query($query);

		$data = array();
		if( $res_query ){
			$data['error'] 	= false;
			$data['msj'] 	= "Empleado eliminado correctamente";
			$data['type'] 	= "success" ;
		}else{
			$data['error'] 	= true;
			$data['msj']	= "No se puso eliminar el empleado";
			$data['type'] 	= "error" ;
		}
		return $data;
	}

}

