$('#frm').submit(function (e) { 
    e.preventDefault();
    let datos = $( this ).serializeArray();
        datos.push({name: 'id' , value: id })

    let roles = new Array();
    $(".roles").each(function(){
        roles.push({name: $(this).attr('id') , value: $(this).prop('checked') })
    });
    console.log(roles)

    $.ajax({
        type: 'post',
        url: 'controllers/empleados.php',
        dataType: 'json',
        data: {datos: datos, roles:roles, opcn: opcn},
    }).done(function (data) {
        swal(data.msj,'',data.type);
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
});

$("body").on("click", ".swal-button--confirm", function (e) {
    location.reload();
  });


  $('.eliminar').click(function (e) { 
      e.preventDefault();

      if( !confirm('¿Esta seguro de eliminar este empleado?')){ return false;}
      let id = $( this ).data('id')
      $.ajax({
        type: 'post',
        url: 'controllers/empleados.php',
        dataType: 'json',
        data: {opcn: "eliminar", id:id},
    }).done(function (data) {
        swal(data.msj,'',data.type);
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
  });