<?php include('controllers/empleados.php'); ?>
<?php include('../template/header.php'); ?>
<div class="container">
  <h1>Modificar empleado</h1>
  <div class="alert alert-info" role="alert">Los campos con (*) son obligatorios</div>
  <form class="form-horizontal" id="frm">
    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Nombre completo *</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre completo del empleado" required value="<?php echo $empleado['nombre'] ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Correo electrónico *</label>
      <div class="col-sm-10">
        <input type="email" class="form-control" id="email" name="email" placeholder="Correo electrónico" required value="<?php echo $empleado['email'] ?>">
      </div>
    </div>

    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Sexo *</label>
      <div class="col-sm-10">
        <div class="radio">
          <label>
            <input type="radio" id="sexom" name="sexo" value="M" required <?php if( $empleado['sexo'] == "M" ){ echo "checked"; } ?> > Masculino
          </label>
        </div>
        <div class="radio">
          <label>
            <input type="radio" id="sexof" name="sexo" value="F" required <?php if( $empleado['sexo'] == "F" ){ echo "checked"; } ?>> Femenino
          </label>
        </div>
      </div>
    </div>

    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label">Area *</label>
      <div class="col-sm-10">
        <select class="form-control" name="area_id" id="area_id" required style="width: 100%;" required>
          <option value="">Seleccionar</option>
          <?php foreach ($areas as $key => $value) { ?>
            <option value="<?php echo $value['id'] ?>" <?php if( $empleado['area_id'] == $value['id'] ){ echo "selected"; } ?> ><?php echo $value['nombre'] ?></option>
          <?php } ?>
        </select>
      </div>
    </div>

    <div class="form-group">
      <label for="inputEmail3" class="col-sm-2 control-label">Descripción *</label>
      <div class="col-sm-10">
        <textarea id="descripcion" name="descripcion" class="form-control" rows="3" placeholder="Descripción de la experiencia del empleado" required><?php echo $empleado['descripcion'] ?></textarea>
      </div>
    </div>

    <div class="form-group">
      <label for="inputPassword3" class="col-sm-2 control-label"></label>
      <div class="col-sm-10">
        <div class="checkbox">
          <label>
            <input type="checkbox" id="boletin" name="boletin" <?php if( $empleado['boletin'] == 1 ){ echo "checked"; } ?>> Deseo recibir boletín informativo
          </label>
        </div>
      </div>
    </div>
    <div class="form-group">
      <label class="col-sm-2 control-label">Roles *</label>
      <div class="col-sm-10">
        <?php foreach ($roleEmp as $key => $value) { ?>
          <div class="checkbox">
            <label>
              <input class="roles" type="checkbox" id="<?php echo 'rol' . $value['id'] ?>" name="<?php echo 'rol' . $value['id'] ?>" <?php if( $value['empleado_id'] != '' ){ echo "checked"; } ?> > <?php echo $value['nombre'] ?>
            </label>
          </div>
        <?php } ?>
      </div>
    </div>

    <div class="form-group">
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Guardar</button>
        <a class="btn btn-warning" href="index.php"><i class="fa fa-chevron-left fa-fw"></i>Volver</a>
      </div>
    </div>
  </form>
  <!-- Fin formulario crear empleado -->
</div>
<?php include('../template/footer.php'); ?>
<script>
let opcn = "editar"
let id = <?php echo $empleado['id']; ?>
</script>
<script src="js/empleados.js?sin_cache=<?php echo md5(time()); ?>"></script>